
const findingTheCarById = (requiredId, inventory) => {
  if (Array.isArray(inventory)) {
      for (let carId = 0; carId < inventory.length; carId++) {
        if (inventory[carId].id === requiredId) {
          return inventory[carId];
        }
      }
  }
  return null; // Return null if the car with the specified id is not found
}


module.exports = findingTheCarById