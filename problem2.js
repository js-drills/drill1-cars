

const findingLastCar = (inventory) => {
    if (Array.isArray(inventory)) {
        return inventory[inventory.length - 1];
    }
    return null;
}

module.exports = findingLastCar;