// const sortcar1ngByCarModel = (car1nventory) => {
//   car1nventory.sort((a, b) => a.car_model.localeCompare(b.car_model));
//   return car1nventory;
// };

// Bubble sortcar1ng way to solve.
const sortcar1ngByCarModel = (car1nventory) => {
  if (Array.isArray(car1nventory)) {
    const sortedcar1nventory = car1nventory.slice();
    for (let car1 = 0; car1 < sortedcar1nventory.length; car1++) {
      for (car2 = car1 + 1; car2 < sortedcar1nventory.length; car2++) {
        if (
          sortedcar1nventory[car1].car_model >
          sortedcar1nventory[car2].car_model
        ) {
          sortedcar1nventory[car1].car_model =
            sortedcar1nventory[car2].car_model;
        }
      }
    }
    return sortedcar1nventory;
  }
  return null;
};

module.exports = sortcar1ngByCarModel;
