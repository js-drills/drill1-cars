
const onlyCarYears = (inventory) => {
    if (Array.isArray(inventory)) {
        let years = [];
        for (car = 0; car < inventory.length; car++) {
          years[car] = inventory[car].car_year;
        }
        return years;
    }
    return null;
}

module.exports = onlyCarYears