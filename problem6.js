
let BMWandAudi = (inventory) => {
    if (Array.isArray(inventory)) {
        let bmwandaudi = [];
        for (let car = 0; car < inventory.length; car++) {
          if (
            inventory[car].car_make === "BMW" ||
            inventory[car].car_make === "Audi"
          ) {
            bmwandaudi.push(inventory[car]);
          }
        }
        return bmwandaudi;
    }
    return null;
}

module.exports = BMWandAudi